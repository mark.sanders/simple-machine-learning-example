# %% IMPORT

import numpy as np # THE mathematics library for python
# while importing, the name to call the library with can be redefined as well. Too lazy to write "numpy"? Rename it "np"
import pandas as pd # powerful library for working with large datasets - I use it to load a simple CSV...
import pickle # Saving data for further use, hence "pickle"
from tqdm import tqdm # Nice progressbar for long running tasks

print("Finished importing stuff")

# %% LOAD CSV

print("Loading 100MB CSV")

# Use pandas' csv_read function, which outputs a so-called dataframe - see below, what that can do
csv = pd.read_csv("./data_train.csv", encoding="ISO-8859-1")

print("DONE")

# %% formatting data

x = [csv[
            ["position"] +
            list(csv.columns[list(csv.columns).index("A Z-Axis"):list(csv.columns).index("M Scale Y-Achse Case")+1])
        ]
    ]
print("This is the X part to train the algorithms on:\n", x)

y = {}
mult = 1e6
print("FROM HERE ON, ALL Y-VALUES ARE IN µm, as there used to be problems with precision with very small y-values")
for dataset in [dataset for dataset in list(csv.columns) if "E" in dataset]:
    y[dataset] = csv[[dataset]].values*mult

print("And those are the Y-Vectors:\n")
for i in y:
    print(i, y[i], "\n\n")

# %% simple trining

from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
# x_scaled = sc.fit_transform(x[0])

regressor_model = MLPRegressor(hidden_layer_sizes=(100, ), max_iter=20, verbose=1, alpha=0.001, batch_size='auto', learning_rate='constant', learning_rate_init=0.01)
regressor_model.fit(x[0], y["EXX"].ravel())

