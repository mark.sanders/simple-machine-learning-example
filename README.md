# Simple Machine Learning example

Machine learning for machine tool compensation.

## Want to do this yourself?
Install anaconda & follow one of the countless guides online, e.g.
https://jupyter.readthedocs.io/en/latest/install.html#installing-jupyter-using-anaconda-and-conda